package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh01N017 {
    public static double exampleO(double x) {
        return Math.sqrt(1 - Math.pow(Math.sin(x), 2));
    }

    public static double exampleP(double x, double a, double b, double c) {
        return 1 / Math.sqrt(a * Math.pow(x, 2) + b * x + c);
    }

    public static double exampleR(double x) {
        return (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / (2 * Math.sqrt(x));
    }

    public static double exampleS(double x) {
        return Math.abs(x) + Math.abs(x + 1);
    }
}
