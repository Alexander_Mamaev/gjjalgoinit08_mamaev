package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

import java.util.Scanner;

public class TaskCh12N025 {
    private static Scanner scanner = new Scanner(System.in);
    private static boolean direction = true;

    public static void main(String[] args) {
        System.out.println("Please enter the array type (A, B, V, G, D, E, J, Z, I, K, L, M, N, O, P, R)");
        String arrayType = scanner.nextLine();
        System.out.println("Please enter the array length (Width x Height).");
        System.out.print("Width = ");
        int width = readInt(1, Integer.MAX_VALUE);
        System.out.print("Height = ");
        int height = readInt(1, Integer.MAX_VALUE);
        printArray(arrayType, height, width);
    }

    private static int[][] getArrayA(int height, int width) {
        int[][] array = new int[height][width];
        for (int k = 1, j = 0; j < array.length; j++) {
            for (int i = 0; i < array[j].length; i++) {
                array[j][i] = k++;
            }
        }
        return array;
    }

    private static int[][] getArrayB(int height, int width) {
        int[][] array = new int[height][width];
        for (int k = 1, i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[j][i] = k++;
            }
        }
        return array;
    }

    private static int[][] getArrayV(int height, int width) {
        int[][] array = new int[height][width];
        for (int k = 1, j = 0; j < array.length; j++) {
            for (int i = array[j].length - 1; i >= 0; i--) {
                array[j][i] = k++;
            }
        }
        return array;
    }

    private static int[][] getArrayG(int height, int width) {
        int[][] array = new int[height][width];
        for (int k = 1, i = 0; i < array[0].length; i++) {
            for (int j = array.length - 1; j >= 0; j--) {
                array[j][i] = k++;
            }
        }
        return array;
    }

    private static int[][] getArrayD(int height, int width) {
        int[][] array = new int[height][width];
        clearDirection();
        for (int k = 1, j = 0; j < array.length; j++) {
            if (direction) {
                for (int i = 0; i < array[j].length; i++) {
                    array[j][i] = k++;
                }
            } else {
                for (int i = array[j].length - 1; i >= 0; i--) {
                    array[j][i] = k++;
                }
            }
            changeDirection();
        }
        return array;
    }

    private static int[][] getArrayE(int height, int width) {
        int[][] array = new int[height][width];
        clearDirection();
        for (int k = 1, i = 0; i < array[0].length; i++) {
            if (direction) {
                for (int j = 0; j < array.length; j++) {
                    array[j][i] = k++;
                }
            } else {
                for (int j = array.length - 1; j >= 0; j--) {
                    array[j][i] = k++;
                }
            }
            changeDirection();
        }
        return array;
    }

    private static int[][] getArrayJ(int height, int width) {
        int[][] array = new int[height][width];
        for (int k = 1, j = array.length - 1; j >= 0; j--) {
            for (int i = 0; i < array[j].length; i++) {
                array[j][i] = k++;
            }
        }
        return array;
    }

    private static int[][] getArrayZ(int height, int width) {
        int[][] array = new int[height][width];
        for (int k = 1, i = array[0].length - 1; i >= 0; i--) {
            for (int j = 0; j < array.length; j++) {
                array[j][i] = k++;
            }
        }
        return array;
    }

    private static int[][] getArrayI(int height, int width) {
        int[][] array = new int[height][width];
        for (int k = 1, j = array.length - 1; j >= 0; j--) {
            for (int i = array[j].length - 1; i >= 0; i--) {
                array[j][i] = k++;
            }
        }
        return array;
    }

    private static int[][] getArrayK(int height, int width) {
        int[][] array = new int[height][width];
        for (int k = 1, i = array[0].length - 1; i >= 0; i--) {
            for (int j = array.length - 1; j >= 0; j--) {
                array[j][i] = k++;
            }
        }
        return array;
    }

    private static int[][] getArrayL(int height, int width) {
        int[][] array = new int[height][width];
        clearDirection();
        for (int k = 1, j = array.length - 1; j >= 0; j--) {
            if (direction) {
                for (int i = 0; i < array[j].length; i++) {
                    array[j][i] = k++;
                }
            } else {
                for (int i = array[j].length - 1; i >= 0; i--) {
                    array[j][i] = k++;
                }
            }
            changeDirection();
        }
        return array;
    }

    private static int[][] getArrayM(int height, int width) {
        int[][] array = new int[height][width];
        clearDirection();
        changeDirection();
        for (int k = 1, j = 0; j < array.length; j++) {
            if (direction) {
                for (int i = 0; i < array[j].length; i++) {
                    array[j][i] = k++;
                }
            } else {
                for (int i = array[j].length - 1; i >= 0; i--) {
                    array[j][i] = k++;
                }
            }
            changeDirection();
        }
        return array;
    }

    private static int[][] getArrayN(int height, int width) {
        int[][] array = new int[height][width];
        clearDirection();
        for (int k = 1, i = array[0].length - 1; i >= 0; i--) {
            if (direction) {
                for (int j = 0; j < array.length; j++) {
                    array[j][i] = k++;
                }
            } else {
                for (int j = array.length - 1; j >= 0; j--) {
                    array[j][i] = k++;
                }
            }
            changeDirection();
        }
        return array;
    }

    private static int[][] getArrayO(int height, int width) {
        int[][] array = new int[height][width];
        clearDirection();
        changeDirection();
        for (int k = 1, i = 0; i < array[0].length; i++) {
            if (direction) {
                for (int j = 0; j < array.length; j++) {
                    array[j][i] = k++;
                }
            } else {
                for (int j = array.length - 1; j >= 0; j--) {
                    array[j][i] = k++;
                }
            }
            changeDirection();
        }
        return array;
    }

    private static int[][] getArrayP(int height, int width) {
        int[][] array = new int[height][width];
        clearDirection();
        changeDirection();
        for (int k = 1, j = array.length - 1; j >= 0; j--) {
            if (direction) {
                for (int i = 0; i < array[j].length; i++) {
                    array[j][i] = k++;
                }
            } else {
                for (int i = array[j].length - 1; i >= 0; i--) {
                    array[j][i] = k++;
                }
            }
            changeDirection();
        }
        return array;
    }

    private static int[][] getArrayR(int height, int width) {
        int[][] array = new int[height][width];
        clearDirection();
        changeDirection();
        for (int k = 1, i = array[0].length - 1; i >= 0; i--) {
            if (direction) {
                for (int j = 0; j < array.length; j++) {
                    array[j][i] = k++;
                }
            } else {
                for (int j = array.length - 1; j >= 0; j--) {
                    array[j][i] = k++;
                }
            }
            changeDirection();
        }
        return array;
    }

    private static void printArray(String arrayType, int height, int width) {
        switch (arrayType) {
            case "A":
                printArray(getArrayA(height, width));
                break;
            case "B":
                printArray(getArrayB(height, width));
                break;
            case "V":
                printArray(getArrayV(height, width));
                break;
            case "G":
                printArray(getArrayG(height, width));
                break;
            case "D":
                printArray(getArrayD(height, width));
                break;
            case "E":
                printArray(getArrayE(height, width));
                break;
            case "J":
                printArray(getArrayJ(height, width));
                break;
            case "Z":
                printArray(getArrayZ(height, width));
                break;
            case "I":
                printArray(getArrayI(height, width));
                break;
            case "K":
                printArray(getArrayK(height, width));
                break;
            case "L":
                printArray(getArrayL(height, width));
                break;
            case "M":
                printArray(getArrayM(height, width));
                break;
            case "N":
                printArray(getArrayN(height, width));
                break;
            case "O":
                printArray(getArrayO(height, width));
                break;
            case "P":
                printArray(getArrayP(height, width));
                break;
            case "R":
                printArray(getArrayR(height, width));
                break;
            default:
                System.out.println("Incorrect array type!");
                break;
        }
    }

    private static void printArray(int[][] array) {
        for (int[] firstArrayElement : array) {
            for (int secondArrayElement : firstArrayElement) {
                System.out.print(secondArrayElement + " ");
            }
            System.out.println();
        }
    }

    private static void changeDirection() {
        direction = !direction;
    }

    private static void clearDirection() {
        direction = true;
    }
}
