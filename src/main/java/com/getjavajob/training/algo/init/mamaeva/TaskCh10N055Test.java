package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N055.getChangedSystemNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N055Test {
    public static void main(String[] args) {
        getChangedSystemNumberTest(31, 16, "1f");
        getChangedSystemNumberTest(27, 15, "1c");
        getChangedSystemNumberTest(27, 14, "1d");
        getChangedSystemNumberTest(27, 13, "21");
        getChangedSystemNumberTest(22, 12, "1a");
        getChangedSystemNumberTest(21, 11, "1a");
        getChangedSystemNumberTest(15, 10, "15");
        getChangedSystemNumberTest(15, 9, "16");
        getChangedSystemNumberTest(15, 8, "17");
        getChangedSystemNumberTest(15, 7, "21");
        getChangedSystemNumberTest(15, 6, "23");
        getChangedSystemNumberTest(15, 5, "30");
        getChangedSystemNumberTest(15, 4, "33");
        getChangedSystemNumberTest(15, 3, "120");
        getChangedSystemNumberTest(15, 2, "1111");
        printTestResult();
    }

    private static void getChangedSystemNumberTest(int number, int radix, String expectedResult) {
        assertEquals("TaskCh10N055: getChangedSystemNumberTest: ", expectedResult, getChangedSystemNumber(number, radix));
    }
}
