package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh09N017.isFirstAndLastCharEquals;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh09N017Test {
    public static void main(String[] args) {
        System.out.print("The entered word have the same first and last character?");
        System.out.println();
        isFirstAndLastCharEqualsTest("asdfg", false);
        isFirstAndLastCharEqualsTest("asdfa", true);
        isFirstAndLastCharEqualsTest("a", true);
        printTestResult();
    }

    private static void isFirstAndLastCharEqualsTest(String word, boolean expectedResult) {
        String assertMessage = "TaskCh09N017: Word \"" + word + "\"";
        assertEquals(assertMessage, expectedResult, isFirstAndLastCharEquals(word));
    }
}
