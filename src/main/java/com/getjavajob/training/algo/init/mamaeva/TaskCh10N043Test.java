package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N043.getDigitsQuantity;
import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N043.getDigitsSum;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N043Test {
    public static void main(String[] args) {
        getDigitsSumTest(0, 0);
        getDigitsSumTest(1, 1);
        getDigitsSumTest(102, 3);
        getDigitsSumTest(99, 18);

        getDigitsQuantityTest(0, 1);
        getDigitsQuantityTest(5, 1);
        getDigitsQuantityTest(10, 2);
        getDigitsQuantityTest(1234, 4);

        printTestResult();
    }

    private static void getDigitsSumTest(int n, int expectedResult) {
        assertEquals("TaskCh10N043: getDigitsSumTest ", expectedResult, getDigitsSum(n));
    }

    private static void getDigitsQuantityTest(int n, int expectedResult) {
        assertEquals("TaskCh10N043: getDigitsQuantityTest", expectedResult, getDigitsQuantity(n));
    }
}
