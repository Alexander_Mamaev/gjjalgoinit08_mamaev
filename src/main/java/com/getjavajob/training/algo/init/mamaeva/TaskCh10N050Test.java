package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N050.akkermanFunction;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N050Test {
    public static void main(String[] args) {
        akkermanFunctionTest(0, 0, 1);
        akkermanFunctionTest(1, 1, 3);
        akkermanFunctionTest(2, 2, 7);
        akkermanFunctionTest(1, 3, 5);
        akkermanFunctionTest(3, 5, 253);
        printTestResult();
    }

    private static void akkermanFunctionTest(int n, int m, int expectedResult) {
        assertEquals("TaskCh10N050: akkermanFunctionTest: ", expectedResult, akkermanFunction(n, m));
    }
}
