package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh04N115.yearNameColor;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh04N115Test {
    public static void main(String[] args) {
        seasonTest(1984, "Rat, Green");
        seasonTest(1900, "Rat, White");
        seasonTest(2000, "Dragon, White");
        seasonTest(2016, "Monkey, Red");
        printTestResult();
    }

    private static void seasonTest(int year, String expectedResult) {
        String actualResult = yearNameColor(year);
        String assertMessage = "TaskCh04N115: The year " + year + " is ...";
        assertEquals(assertMessage, expectedResult, actualResult);
    }
}
