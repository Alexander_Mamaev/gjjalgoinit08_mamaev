package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readDouble;

public class TaskCh01N003 {
    public static void main(String[] args) {
        System.out.println("Please enter a number (Double).");
        System.out.println(printNumber(readDouble()));
    }

    private static String printNumber(double inputNumber) {
        return "You enter the number " + inputNumber;
    }
}
