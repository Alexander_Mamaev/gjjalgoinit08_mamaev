package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh12N024.getArrayA;
import static com.getjavajob.training.algo.init.mamaeva.TaskCh12N024.getArrayB;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh12N024Test {
    public static void main(String[] args) {
        int[][] arrayA = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        int[][] arrayB = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        };
        getArrayTest(getArrayA(arrayA.length), arrayA);
        getArrayTest(getArrayB(arrayB.length), arrayB);
        printTestResult();
    }

    private static void getArrayTest(int[][] array, int[][] expectedResult) {
        assertEquals("TaskCh12N024: getArrayTest: ", expectedResult, array);
    }
}
