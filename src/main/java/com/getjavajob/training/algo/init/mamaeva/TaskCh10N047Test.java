package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N047.getFibonacciTerm;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N047Test {
    public static void main(String[] args) {
        getFibonacciTermTest(1, 1);
        getFibonacciTermTest(2, 1);
        getFibonacciTermTest(3, 2);
        getFibonacciTermTest(4, 3);
        getFibonacciTermTest(5, 5);
        getFibonacciTermTest(6, 8);
        printTestResult();
    }

    private static void getFibonacciTermTest(int kTerm, int expectedResult) {
        assertEquals("TaskCh10N047: getFibonacciTermTest: ", expectedResult, getFibonacciTerm(kTerm));
    }
}
