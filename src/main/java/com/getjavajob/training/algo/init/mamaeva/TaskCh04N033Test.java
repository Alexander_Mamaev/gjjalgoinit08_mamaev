package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh04N033.isLastNumberEven;
import static com.getjavajob.training.algo.init.mamaeva.TaskCh04N033.isLastNumberOdd;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh04N033Test {
    public static void main(String[] args) {
        isLastNumberEvenTest(243, false);
        isLastNumberOddTest(243, true);
        isLastNumberEvenTest(234, true);
        isLastNumberOddTest(234, false);
        printTestResult();
    }

    private static void isLastNumberEvenTest(int number, boolean expectedResult) {
        boolean actualResult = isLastNumberEven(number);
        String assertMessage = "TaskCh04N033: Last number of " + number + " is even? ";
        assertEquals(assertMessage, expectedResult, actualResult);
    }

    private static void isLastNumberOddTest(int number, boolean expectedResult) {
        boolean actualResult = isLastNumberOdd(number);
        String assertMessage = "TaskCh04N033: Last number of " + number + " is odd? ";
        assertEquals(assertMessage, expectedResult, actualResult);
    }
}
