package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N041 {
    public static int getFactorial(int n) {
        if (n == 1) {
            return n;
        } else {
            return n * getFactorial(n - 1);
        }
    }
}
