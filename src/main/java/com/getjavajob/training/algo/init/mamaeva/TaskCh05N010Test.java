package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh05N010.dollarExchangeTable;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh05N010Test {
    public static void main(String[] args) {
        double dollarExchangeRate = 2;
        double[] dollarTable = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        double[][] expectedDollarExchangeTable = {
                {1.0, 2.0},
                {2.0, 4.0},
                {3.0, 6.0},
                {4.0, 8.0},
                {5.0, 10.0},
                {6.0, 12.0},
                {7.0, 14.0},
                {8.0, 16.0},
                {9.0, 18.0},
                {10.0, 20.0},
                {11.0, 22.0},
                {12.0, 24.0},
                {13.0, 26.0},
                {14.0, 28.0},
                {15.0, 30.0},
                {16.0, 32.0},
                {17.0, 34.0},
                {18.0, 36.0},
                {19.0, 38.0},
                {20.0, 40.0}
        };
        dollarExchangeTableTest(dollarExchangeRate, dollarTable, expectedDollarExchangeTable);
        printTestResult();
    }

    private static void dollarExchangeTableTest(double exchangeRate, double[] dollarTable, double[][] expectedResult) {
        double[][] actualResult = dollarExchangeTable(dollarTable, exchangeRate);
        assertEquals("TaskCh05N010: ", expectedResult, actualResult);
    }
}
