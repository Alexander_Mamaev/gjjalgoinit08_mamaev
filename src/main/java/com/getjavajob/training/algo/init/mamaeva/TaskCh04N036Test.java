package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh04N036.trafficLight;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh04N036Test {
    public static void main(String[] args) {
        trafficLightTest(3, "red");
        trafficLightTest(5, "green");
        trafficLightTest(13.5, "red");
        trafficLightTest(12.3456, "green");
        printTestResult();
    }

    private static void trafficLightTest(double number, String expectedResult) {
        String actualResult = trafficLight(number);
        String assertMessage = "TaskCh04N036: The traffic light at " + number + " minutes is...";
        assertEquals(assertMessage, expectedResult, actualResult);
    }
}
