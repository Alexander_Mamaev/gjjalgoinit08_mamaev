package com.getjavajob.training.algo.init.mamaeva;

import java.math.BigDecimal;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh05N064.averagePopulationDensity;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh05N064Test {
    public static void main(String[] args) {
        int[][] populationAndArea = {
                {12, 60},
                {11, 55},
                {10, 50},
                {9, 45},
                {8, 40},
                {7, 35},
                {6, 30},
                {5, 25},
                {4, 20},
                {3, 15},
                {2, 10},
                {1, 5}
        };
        averagePopulationDensityTest(populationAndArea, 0.2);
        printTestResult();
    }

    private static void averagePopulationDensityTest(int[][] populationAndAreaTable, double expectedResult) {
        double actualRoundResult = new BigDecimal(averagePopulationDensity(populationAndAreaTable)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double expectedRoundResult = new BigDecimal(expectedResult).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        String assertMessage = "TaskCh05N064: The average population density is...";
        assertEquals(assertMessage, expectedRoundResult, actualRoundResult);
    }
}
