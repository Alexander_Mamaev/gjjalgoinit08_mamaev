package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh09N015.charAtPosition;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh09N015Test {
    public static void main(String[] args) {
        charAtPositionTest("abcdefg", 3, 'c');
        charAtPositionTest("abcdefg", 1, 'a');
        charAtPositionTest("abcdefg", 7, 'g');
        printTestResult();
    }

    private static void charAtPositionTest(String word, int charPosition, char expectedResult) {
        String assertMessage = "TaskCh09N015: Word " + word + ". Char position " + charPosition + ".";
        assertEquals(assertMessage, expectedResult, charAtPosition(word, charPosition));
    }
}
