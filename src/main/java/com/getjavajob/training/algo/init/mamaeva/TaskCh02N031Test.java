package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh02N031.getInitialNumberOrder;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh02N031Test {
    public static void main(String[] args) {
        changeNumberOrderTest(567, 576);
        changeNumberOrderTest(100, 100);
        changeNumberOrderTest(210, 201);
        changeNumberOrderTest(230, 203);
        changeNumberOrderTest(999, 999);
        printTestResult();
    }

    private static void changeNumberOrderTest(int changedNumberOrder, int expectedInitialNumberOrder) {
        String assertMessage = "TaskCh02N031: " + changedNumberOrder + " --> " + expectedInitialNumberOrder;
        int actualNumberOrder = getInitialNumberOrder(changedNumberOrder);
        assertEquals(assertMessage, expectedInitialNumberOrder, actualNumberOrder);
    }


}
