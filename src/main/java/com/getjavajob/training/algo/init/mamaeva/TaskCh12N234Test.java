package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh12N234.delColumn;
import static com.getjavajob.training.algo.init.mamaeva.TaskCh12N234.delRaw;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh12N234Test {
    public static void main(String[] args) {
        int[][] array = new int[][]{
                {10, 2, 10, 20},
                {2, 2, 2, 2},
                {15, 2, 20, 15},
                {20, 2, 10, 20}
        };
        int[][] delRaw2ExpectedResult = new int[][]{
                {10, 2, 10, 20},
                {15, 2, 20, 15},
                {20, 2, 10, 20},
                {0, 0, 0, 0}
        };

        int[][] delCol2ExpectedResult = new int[][]{
                {10, 10, 20, 0},
                {2, 2, 2, 0},
                {15, 20, 15, 0},
                {20, 10, 20, 0}
        };

        delRawTest(array, 2, delRaw2ExpectedResult);
        delColTest(array, 2, delCol2ExpectedResult);
        printTestResult();
    }

    private static void delRawTest(int[][] array, int rawNumber, int[][] expectedResult) {
        assertEquals("TaskCh12N234: delRawTest: ", expectedResult, delRaw(array, rawNumber));
    }

    private static void delColTest(int[][] array, int colNumber, int[][] expectedResult) {
        assertEquals("TaskCh12N234: delCalTest: ", expectedResult, delColumn(array, colNumber));
    }
}
