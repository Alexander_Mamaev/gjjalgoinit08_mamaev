package com.getjavajob.training.algo.init.mamaeva;

import java.util.Scanner;

public class TaskCh09N185 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Please enter an arithmetic expression.");
        printResult(isAllBracketsCorrect(scanner.nextLine()));
    }

    /**
     * @return
     * {0,0} if all right
     * {1, x} if left brackets are excess, x - amount of excess brackets
     * {2, y} if right bracket is excess, y - the position of excess bracket (position starts from 1)
     */
    public static int[] isAllBracketsCorrect(String expression) {
        char[] exprArray = expression.toCharArray();
        int countUnclosedBrackets = 0;
        for (int i = 0; i < exprArray.length; i++) {
            if (exprArray[i] == '(') {
                countUnclosedBrackets++;
            } else if (exprArray[i] == ')') {
                countUnclosedBrackets--;
                if (countUnclosedBrackets < 0) {
                    return new int[]{2, ++i};
                }
            }
        }
        return countUnclosedBrackets > 0 ? new int[]{1, countUnclosedBrackets} : new int[]{0, 0};
    }

    private static void printResult(int[] result) {
        switch (result[0]) {
            case 0:
                System.out.println("Yes. Correct expression.");
                break;
            case 1:
                System.out.println("No. Left brackets are excess. Amount of excess brackets - " + result[1]);
                break;
            case 2:
                System.out.println("No. The position of excess right bracket - " + result[1]);
                break;
            default:
                System.out.println("Something wrong.");
                break;
        }
    }
}
