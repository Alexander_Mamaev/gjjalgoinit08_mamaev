package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh06N008 {
    public static void main(String[] args) {
        System.out.println("Please enter a integer number > 0.");
        int number = readInt(1, Integer.MAX_VALUE);
        printArray(numberSequenceUntilNumber(number));
    }

    public static int[] numberSequenceUntilNumber(int number) {
        int[] numberSequenceBeforeNumber = new int[(int) Math.pow(number, 0.5)];
        for (int i = 0; i < numberSequenceBeforeNumber.length; i++) {
            numberSequenceBeforeNumber[i] = (int) Math.pow(i + 1, 2);
        }
        return numberSequenceBeforeNumber;
    }

    private static void printArray(int[] array) {
        for (int arrayElement : array) {
            System.out.print(arrayElement + " ");
        }
    }

}
