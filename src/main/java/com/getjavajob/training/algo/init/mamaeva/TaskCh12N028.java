package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh12N028 {
    public static void main(String[] args) {
        System.out.println("Please enter the array length (Width x Height).");
        System.out.print("Width = ");
        int width = readInt(1, Integer.MAX_VALUE);
        System.out.print("Height = ");
        int height = readInt(1, Integer.MAX_VALUE);
        printArray(getArray(height, width));
    }

    public static int[][] getArray(int height, int width) {
        int[][] array = new int[height][width];
        int row = 0, column = 0, dx = 1, dy = 0, directionChanges = 0, numberOfSteps = array[0].length;
        for (int i = 0; i < array.length * array[0].length; i++) {
            array[column][row] = i + 1;
            if (--numberOfSteps == 0) {
                numberOfSteps = array[column].length * (directionChanges % 2) + array.length * ((directionChanges + 1) % 2) - directionChanges / 2 - 1;
                int temp = dx;
                dx = -dy;
                dy = temp;
                directionChanges++;
            }
            row += dx;
            column += dy;
        }
        return array;
    }

    private static void printArray(int[][] array) {
        for (int[] firstArrayElement : array) {
            for (int secondArrayElement : firstArrayElement) {
                System.out.print(secondArrayElement + " ");
            }
            System.out.println();
        }
    }
}
