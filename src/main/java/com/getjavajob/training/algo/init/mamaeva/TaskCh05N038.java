package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh05N038 {
    private static int manDirectionToWork = 1;

    public static void main(String[] args) {
        System.out.println("Please enter the turns number.");
        int turnsValue = readInt();
        System.out.println("The number of man turns is " + turnsValue);
        System.out.printf("The distance between man and home is %f kilometers.", distanceBetweenManAndHome(turnsValue));
        System.out.println();
        System.out.printf("The total distance which the man was walked is %f kilometers.", allManDistance(turnsValue));
    }

    public static double distanceBetweenManAndHome(int turnsValue) {
        double distanceBetweenManAndHome = 0;
        for (int turnNumber = 0; turnNumber <= turnsValue; turnNumber++) {
            distanceBetweenManAndHome += getDirection() / (turnNumber + 1.0);
            changeDirection();
        }
        return Math.abs(distanceBetweenManAndHome);
    }

    public static double allManDistance(int turnsValue) {
        double allManDistance = 0;
        for (int turnNumber = 0; turnNumber <= turnsValue; turnNumber++) {
            allManDistance +=  1.0 / (turnNumber + 1);
        }
        return allManDistance;
    }

    private static int getDirection() {
        return manDirectionToWork;
    }

    private static void changeDirection() {
        manDirectionToWork *= -1;
    }
}
