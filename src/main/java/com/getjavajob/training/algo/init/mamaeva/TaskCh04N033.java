package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh04N033 {
    public static void main(String[] args) {
        System.out.println("Please enter a natural (integer) number");
        int number = readInt();
        System.out.println("Last number of " + number + " is even? " + isLastNumberEven(number));
        System.out.println("Last number of " + number + " is odd? " + isLastNumberOdd(number));
    }

    public static boolean isLastNumberEven(int number) {
        return number % 2 == 0;
    }

    public static boolean isLastNumberOdd(int number) {
        return number % 2 != 0;
    }
}

