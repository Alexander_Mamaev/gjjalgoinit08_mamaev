package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh02N043 {
    public static void main(String[] args) {
        System.out.println("Please enter a first non zero integer.");
        int a = inputNumber();
        System.out.println("Please enter a second non zero integer.");
        int b = inputNumber();
        System.out.println("The result is " + divisibility(a, b));
    }

    private static int inputNumber() {
        int number;
        while (true) {
            number = readInt();
            if (number != 0) {
                return number;
            }
        }
    }

    public static int divisibility(int a, int b) {
        return (a % b) * (b % a) + 1;
    }
}
