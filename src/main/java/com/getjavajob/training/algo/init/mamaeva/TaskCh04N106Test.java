package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh04N106.season;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh04N106Test {
    public static void main(String[] args) {
        seasonTest(12, "Winter");
        seasonTest(5, "Spring");
        seasonTest(6, "Summer");
        seasonTest(11, "Autumn");
        printTestResult();
    }

    private static void seasonTest(int monthNumber, String expectedResult) {
        String actualResult = season(monthNumber);
        String assertMessage = "TaskCh04N106: The month number " + monthNumber + " corresponded to...";
        assertEquals(assertMessage, expectedResult, actualResult);
    }
}
