package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh12N024 {
    public static void main(String[] args) {
        System.out.println("Please enter the array length (NxN).");
        int n = readInt(1, Integer.MAX_VALUE);
        System.out.println("--------------ArrayA--------------");
        printArray(getArrayA(n));
        System.out.println("--------------ArrayB--------------");
        printArray(getArrayB(n));

    }

    public static int[][] getArrayA(int arrayLength) {
        int[][] array = new int[arrayLength][arrayLength];
        for (int j = 0; j < array.length; j++) {
            for (int i = 0; i < array[j].length; i++) {
                if (i == 0 || j == 0) {
                    array[j][i] = 1;
                } else {
                    array[j][i] = array[j - 1][i] + array[j][i - 1];
                }
            }
        }
        return array;
    }

    public static int[][] getArrayB(int arrayLength) {
        int[][] array = new int[arrayLength][arrayLength];
        for (int j = 0; j < array.length; j++) {
            for (int i = 0; i < array.length; i++) {
                array[j][i] = 1 + (i + j) % 6;
            }
        }
        return array;
    }

    private static void printArray(int[][] array) {
        for (int[] firstArrayElement : array) {
            for (int secondArrayElement : firstArrayElement) {
                System.out.print(secondArrayElement + " ");
            }
            System.out.println();
        }
    }
}
