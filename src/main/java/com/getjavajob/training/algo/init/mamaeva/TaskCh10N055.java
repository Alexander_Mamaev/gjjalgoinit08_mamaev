package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh10N055 {
    public static void main(String[] args) {
        System.out.println("Please enter an integer number > 0.");
        int number = readInt(1, Integer.MAX_VALUE);
        System.out.println("Please enter a number system from 2 to 16.");
        int radix = readInt(2, 16);
        System.out.println("The number is " + getChangedSystemNumber(number, radix));
    }

    public static String getChangedSystemNumber(int number, int radix) {
        char[] allNumbers = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        if (number == 0) {
            return "";
        } else {
            return getChangedSystemNumber(number / radix, radix) + (allNumbers[number % radix]);
        }
    }
}
