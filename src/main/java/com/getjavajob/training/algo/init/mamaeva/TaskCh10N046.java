package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N046 {
    public static int getGeometricProgressionTerm(int firstTerm, int progressionDenominator, int nTerm) {
        if (nTerm == 1) {
            return firstTerm;
        } else {
            return getGeometricProgressionTerm(firstTerm, progressionDenominator, --nTerm) * progressionDenominator;
        }
    }

    public static int getGeometricProgressionSum(int firstTerm, int progressionDenominator, int nTerm) {
        if (nTerm == 1) {
            return firstTerm;
        } else {
            return getGeometricProgressionTerm(firstTerm, progressionDenominator, nTerm) +
                    getGeometricProgressionSum(firstTerm, progressionDenominator, --nTerm);
        }
    }
}
