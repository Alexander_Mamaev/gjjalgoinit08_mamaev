package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh04N067 {
    public static void main(String[] args) {
        System.out.println("Please enter a day (1 - 365).");
        int dayNumber = readInt(1, 365);
        System.out.println("This day is " + dayType(dayNumber));
    }

    public static String dayType(int dayNumber) {
        int day = dayNumber % 7;
        return day == 0 || day == 6 ? "Weekend" : "Workday";
    }
}
