package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N047 {
    public static int getFibonacciTerm(int kTerm) {
        if (kTerm == 1 || kTerm == 2) {
            return 1;
        } else {
            return getFibonacciTerm(kTerm - 1) + getFibonacciTerm(kTerm - 2);
        }
    }
}
