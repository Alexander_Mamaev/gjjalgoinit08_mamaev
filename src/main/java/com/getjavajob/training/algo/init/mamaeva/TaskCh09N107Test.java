package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh09N107.changeWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh09N107Test {
    public static void main(String[] args) {
        changeWordTest("firsta_lastowordo", "firsto_lastoworda");
        changeWordTest("word", "word");
        changeWordTest("", "");
        printTestResult();
    }

    private static void changeWordTest(String word, String expectedResult) {
        String assertMessage = "TaskCh09N107: Word \"" + word + "\"";
        assertEquals(assertMessage, expectedResult, changeWord(word));
    }
}
