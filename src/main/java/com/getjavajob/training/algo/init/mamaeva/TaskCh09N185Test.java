package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh09N185.isAllBracketsCorrect;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh09N185Test {
    public static void main(String[] args) {
        isAllBracketsCorrectTest("(a(b(c)))", new int[]{0, 0});
        isAllBracketsCorrectTest("(a(b(c)", new int[]{1, 2});
        isAllBracketsCorrectTest("(a(b)))c)", new int[]{2, 7});
        printTestResult();
    }

    private static void isAllBracketsCorrectTest(String expression, int[] expectedResult) {
        assertEquals("TaskCh09N185: ", expectedResult, isAllBracketsCorrect(expression));
    }
}
