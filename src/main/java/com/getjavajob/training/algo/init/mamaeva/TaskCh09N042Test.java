package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh09N042.reverseWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh09N042Test {
    public static void main(String[] args) {
        reverseWordTest("word", "drow");
        reverseWordTest("a", "a");
        printTestResult();
    }

    private static void reverseWordTest(String word, String expectedResult) {
        String assertMessage = "TaskCh09N017: Word \"" + word + "\"";
        assertEquals(assertMessage, expectedResult, reverseWord(word));
    }
}
