package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N050 {
    public static void main(String[] args) {
        System.out.println(akkermanFunction(1, 3));
    }

    public static int akkermanFunction(int n, int m) {
        if (n == 0) {
            return m + 1;
        } else if (m == 0) {
            return akkermanFunction(n - 1, 1);
        } else {
            return akkermanFunction(n - 1, akkermanFunction(n, m - 1));
        }
    }
}
