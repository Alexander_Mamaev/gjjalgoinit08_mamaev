package com.getjavajob.training.algo.init.mamaeva;

import java.util.Scanner;

public class TaskCh09N017 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Please enter a word.");
        String word = scanner.nextLine();
        System.out.print("The entered word have the same first and last character? --> ");
        System.out.println(isFirstAndLastCharEquals(word));
    }

    public static boolean isFirstAndLastCharEquals(String word) {
        return word.charAt(0) == word.charAt(word.length() - 1);
    }
}
