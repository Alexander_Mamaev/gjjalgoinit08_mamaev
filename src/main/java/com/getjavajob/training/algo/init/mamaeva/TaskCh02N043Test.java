package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh02N043.divisibility;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh02N043Test {
    public static void main(String[] args) {
        testDivisibility(12, 24, 1);
        testDivisibility(2, 28, 1);
        testDivisibility(999, 34, 443);
        testDivisibility(-3, 1, 1);
        testDivisibility(-24, -46, 529);
        printTestResult();
    }

    private static void testDivisibility(int a, int b, int expectedResult) {
        boolean actual = divisibility(a, b) == 1;
        boolean expected = expectedResult == 1;
        assertEquals("TaskCh02N043: ", expected, actual);
    }
}
