package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh02N039 {
    public static void main(String[] args) {
        int hours = inputNumber("hours");
        int minutes = inputNumber("minutes");
        int seconds = inputNumber("seconds");
        double degrees = getAngleValue(hours, minutes, seconds);
        System.out.printf("%d Hours %d Minutes %d Seconds equals %f degrees", hours, minutes, seconds, degrees);
    }

    private static int inputNumber(String unit) {
        int number = 0;
        switch (unit) {
            case "hours":
                System.out.println("Please enter a hours from 1 to 23: ");
                number = readInt(1, 23);
                break;
            case "minutes":
                System.out.println("Please enter a minutes from 0 to 59");
                number = readInt(0, 59);
                break;
            case "seconds":
                System.out.println("Please enter a seconds from 0 to 59");
                number = readInt(0, 59);
                break;
            default:
                System.out.println("Something wrong.");
                break;
        }
        return number;
    }

    public static double getAngleValue(int hours, int minutes, int seconds) {
        double angle = (double) (hours * 3600 + minutes * 60 + seconds) * 360 / 43200;
        return angle <= 360 ? angle : angle - 360;
    }
}

