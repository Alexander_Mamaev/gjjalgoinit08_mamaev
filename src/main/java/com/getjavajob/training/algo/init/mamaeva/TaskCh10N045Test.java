package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N045.getArithmeticProgressionSum;
import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N045.getArithmeticProgressionTerm;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N045Test {
    public static void main(String[] args) {
        getArithmeticProgressionTermTest(1, 2, 5, 9);
        getArithmeticProgressionTermTest(5, 5, 3, 15);
        getArithmeticProgressionSumTest(1, 2, 5, 25);
        getArithmeticProgressionSumTest(5, 5, 3, 30);
        printTestResult();
    }

    private static void getArithmeticProgressionTermTest(int firstTerm, int difference, int nTerm, int expectedResult) {
        assertEquals("TaskCh10N045: getArithmeticProgressionTermTest: ", expectedResult, getArithmeticProgressionTerm(firstTerm, difference, nTerm));
    }

    private static void getArithmeticProgressionSumTest(int firstTerm, int difference, int nTerm, int expectedResult) {
        assertEquals("TaskCh10N045: getArithmeticProgressionSumTest: ", expectedResult, getArithmeticProgressionSum(firstTerm, difference, nTerm));
    }
}
