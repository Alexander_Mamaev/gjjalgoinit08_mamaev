package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh11N245 {
    public static int[] getChangedArray(int[] array) {
        int[] changedArray = new int[array.length];
        for (int i = 0, j = 0, k = array.length - 1; i < array.length; i++) {
            if (array[i] < 0) {
                changedArray[j++] = array[i];
            } else {
                changedArray[k--] = array[i];
            }
        }
        return changedArray;
    }
}
