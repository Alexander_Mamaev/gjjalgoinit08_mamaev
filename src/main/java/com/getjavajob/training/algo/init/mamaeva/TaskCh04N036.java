package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh04N036 {
    public static void main(String[] args) {
        System.out.println("How many minutes the traffic light works?");
        double minutes = readInt(0, 60);
        System.out.println("Now the " + trafficLight(minutes) + " light.");
    }

    public static String trafficLight(double minutes) {
        return minutes % 5 < 3 ? "green" : "red";
    }
}
