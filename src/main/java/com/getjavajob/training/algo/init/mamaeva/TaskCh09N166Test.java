package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh09N166.isPhraseReversed;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh09N166Test {
    public static void main(String[] args) {
        isPhraseReverseTest("АРГЕНТИНА МАНИТ НЕГРА", true);
        isPhraseReverseTest("ПОТ КАК ПОТОП", false);
        isPhraseReverseTest("А РОЗА УПАЛА НА ЛАПУ АЗОРА", true);
        printTestResult();
    }

    private static void isPhraseReverseTest(String word, boolean expectedResult) {
        String assertMessage = "TaskCh09N166: The phrase \"" + word + "\" is reversed phrase? ";
        assertEquals(assertMessage, expectedResult, isPhraseReversed(word));
    }
}
