package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N041.getFactorial;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N041Test {
    public static void main(String[] args) {
        getFactorialTest(1, 1);
        getFactorialTest(2, 2);
        getFactorialTest(3, 6);
        getFactorialTest(4, 24);
        printTestResult();
    }

    private static void getFactorialTest(int n, int expectedResult) {
        assertEquals("TaskCh10N041: getFactorialTest ", expectedResult, getFactorial(n));
    }
}
