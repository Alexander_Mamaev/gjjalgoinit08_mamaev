package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N052.reverseInteger;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N052Test {
    public static void main(String[] args) {
        reverseIntegerTest(123450, 54321);
        printTestResult();
    }

    private static void reverseIntegerTest(int number, int expectedResult) {
        assertEquals("TaskCh10N052: reverseIntegerTest: ", expectedResult, reverseInteger(number));
    }
}
