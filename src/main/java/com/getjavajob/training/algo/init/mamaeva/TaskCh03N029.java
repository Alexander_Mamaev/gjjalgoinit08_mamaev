package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh03N029 {
    /**
     * a) each of the numbers X and Y is odd.
     * @return true
     */
    public static boolean taskA(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /**
     * b)Only one of the numbers X and Y is less than 20.
     * @return true
     */
    public static boolean taskB(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * c) at least one of the numbers X and Y equal to zero.
     * @return true
     */
    public static boolean taskC(int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * d) each of the numbers X, Y, Z are negative.
     * @return true
     */
    public static boolean taskD(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * e) Only one of the numbers X, Y and Z is multiple of 5.
     * @return true
     */
    public static boolean taskE(int x, int y, int z) {
        boolean isXMultiple = x % 5 == 0;
        boolean isYMultiple = y % 5 == 0;
        boolean isZMultiple = z % 5 == 0;
        return (isXMultiple ^ isYMultiple ^ isZMultiple) && !(isXMultiple && isYMultiple);
    }

    /**
     * f) at least one of the numbers X, Y, Z is more than 100.
     * @return true
     */
    public static boolean taskF(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}
