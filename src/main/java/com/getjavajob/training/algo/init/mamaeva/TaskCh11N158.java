package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh11N158 {
    public static int[] delDuplicates(int[] array) {
        int[] changedArray = new int[array.length];
        int i = 0;
        boolean firsZeroExist = false;
        for (int arrayElement : array) {
            if (arrayElement == 0 && !firsZeroExist) {
                changedArray[i++] = 0;
                firsZeroExist = true;
            } else if (!isNumberExist(arrayElement, changedArray)) {
                changedArray[i++] = arrayElement;
            }
        }
        return changedArray;
    }

    private static boolean isNumberExist(int n, int[] array) {
        for (int arrayElement : array) {
            if (arrayElement == n) {
                return true;
            }
        }
        return false;
    }
}
