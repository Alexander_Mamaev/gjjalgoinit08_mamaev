package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh06N087Test {
    public static void main(String[] args) {
        Game game = new Game();
        System.out.println("Playing the White team VS Black team.");
        game.setTeam1(new Team("WhiteTeam"));
        game.setTeam2(new Team("BlackTeam"));

        int[][] gameArray1 = {{1, 2}, {2, 3}, {2, 1}, {1, 2}, {1, 0}};
        int[] expectedResult1 = {0, 0, 4, 4};
        playGameTest(gameArray1, game, expectedResult1);

        game = new Game();
        System.out.println();
        System.out.println("Playing the White team VS Black team.");
        game.setTeam1(new Team("WhiteTeam"));
        game.setTeam2(new Team("BlackTeam"));

        int[][] gameArray2 = {{1, 3}, {2, 3}, {2, 1}, {1, 2}, {1, 3}, {2, 0}};
        int[] expectedResult2 = {1, 2, 8, 4};
        playGameTest(gameArray2, game, expectedResult2);

        game = new Game();
        System.out.println();
        System.out.println("Playing the White team VS Black team.");
        game.setTeam1(new Team("WhiteTeam"));
        game.setTeam2(new Team("BlackTeam"));

        int[][] gameArray3 = {{1, 2}, {2, 3}, {2, 1}, {1, 0}, {1, 0}, {1, 3}, {1, 3}, {1, 3}};
        int[] expectedResult3 = {2, 1, 4, 2};
        playGameTest(gameArray3, game, expectedResult3);

        printTestResult();
    }

    private static void playGameTest(int[][] gameArray, Game game, int[] expectedResult) {
        for (int[] arrayElement : gameArray) {
            if (arrayElement[1] == 0) {
                break;
            } else if (arrayElement[0] == 1) {
                game.setTeam1Score(arrayElement[1]);
            } else {
                game.setTeam2Score(arrayElement[1]);
            }
        }
        System.out.println();
        game.printGameResult();

        String assertMessage = "TaskCh06N087: The game result is...";
        assertEquals(assertMessage, expectedResult, game.getGameResult());
        System.out.println();
    }
}
