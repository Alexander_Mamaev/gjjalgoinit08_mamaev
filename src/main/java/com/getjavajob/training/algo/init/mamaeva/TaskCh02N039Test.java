package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh02N039.getAngleValue;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh02N039Test {
    public static void main(String[] args) {
        testGetAngleValue(12, 0, 0, 360);
        testGetAngleValue(3, 0, 0, 90);
        testGetAngleValue(15, 0, 0, 90);
        testGetAngleValue(0, 0, 0, 0);
        printTestResult();
    }

    private static void testGetAngleValue(int hours, int minutes, int seconds, double expectedAngleValue) {
        double actualAngleValue = getAngleValue(hours, minutes, seconds);
        assertEquals("TaskCh02N039: ", expectedAngleValue, actualAngleValue);
    }
}
