package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh04N015 {
    public static void main(String[] args) {
        int currentMonth = inputNumber(1);
        int currentYear = inputNumber(2);
        int birthdayMonth = inputNumber(3);
        int birthdayYear = inputNumber(4);
        System.out.println("Person age is " + getPersonAge(currentMonth, currentYear, birthdayMonth, birthdayYear));
    }

    public static int getPersonAge(int currentMonth, int currentYear, int birthdayMonth, int birthdayYear) {
        int difference = currentYear - birthdayYear;
        return currentMonth >= birthdayMonth ? difference : difference - 1;
    }

    private static int inputNumber(int choice) {
        int number = 0;
        switch (choice) {
            case 1:
                System.out.println("Please enter a current month from 1 to 12");
                number = readInt(1, 12);
                break;
            case 2:
                System.out.println("Please enter a current year from 1 to 2016: ");
                number = readInt(1, 2016);
                break;
            case 3:
                System.out.println("Please enter a birthday month from 1 to 12");
                number = readInt(1, 12);
                break;
            case 4:
                System.out.println("Please enter a birthday year from 1 to 2016: ");
                number = readInt(1, 2016);
                break;
            default:
                System.out.println("Something wrong.");
                break;
        }
        return number;
    }
}
