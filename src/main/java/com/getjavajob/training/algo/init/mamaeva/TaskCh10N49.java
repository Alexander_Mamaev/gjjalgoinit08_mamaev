package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N49 {
    public static int getIndexOfMaxArrayElement(int[] array) {
        return getIndexOfMaxArrayElement(array, array.length);
    }

    private static int getIndexOfMaxArrayElement(int[] array, int currentIndexPosition) {
        if (currentIndexPosition == 1) {
            return 0;
        } else {
            int temp = getIndexOfMaxArrayElement(array, --currentIndexPosition);
            return array[currentIndexPosition] > array[temp] ? currentIndexPosition : temp;
        }
    }
}
