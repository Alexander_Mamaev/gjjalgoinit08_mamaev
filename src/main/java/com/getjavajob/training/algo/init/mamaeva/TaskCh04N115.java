package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh04N115 {
    public static void main(String[] args) {
        System.out.println("Please enter the year.");
        int year = readInt();
        System.out.println("This year is ..." + yearNameColor(year));
    }

    public static String yearNameColor(int year) {
        return yearName(year) + ", " + yearColor(year);
    }

    private static String yearName(int year) {
        String yearName = "";
        switch (year % 12) {
            case 0:
                yearName = "Monkey";
                break;
            case 1:
                yearName = "Cock";
                break;
            case 2:
                yearName = "Dog";
                break;
            case 3:
                yearName = "Pig";
                break;
            case 4:
                yearName = "Rat";
                break;
            case 5:
                yearName = "Cow";
                break;
            case 6:
                yearName = "Tiger";
                break;
            case 7:
                yearName = "Rabbit";
                break;
            case 8:
                yearName = "Dragon";
                break;
            case 9:
                yearName = "Snake";
                break;
            case 10:
                yearName = "Horse";
                break;
            case 11:
                yearName = "Sheep";
                break;
            default:
                System.out.println("Something wrong.");
                break;
        }
        return yearName;
    }

    private static String yearColor(int year) {
        String yearColor = "";
        switch (year % 10) {
            case 4:
            case 5:
                yearColor = "Green";
                break;
            case 6:
            case 7:
                yearColor = "Red";
                break;
            case 8:
            case 9:
                yearColor = "Yellow";
                break;
            case 0:
            case 1:
                yearColor = "White";
                break;
            case 2:
            case 3:
                yearColor = "Black";
                break;
            default:
                System.out.println("Something wrong.");
                break;
        }
        return yearColor;
    }
}
