package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh12N023 {
    public static void main(String[] args) {
        System.out.println("Please enter the array length (NxN).");
        int n = readInt(1, Integer.MAX_VALUE);
        System.out.println("--------------ArrayA---------------");
        printArray(getArrayA(n));
        System.out.println("--------------ArrayB---------------");
        printArray(getArrayB(n));
        System.out.println("--------------ArrayC---------------");
        printArray(getArrayC(n));
    }

    public static int[][] getArrayA(int arrayLength) {
        int[][] array = new int[arrayLength][arrayLength];
        for (int j = 0; j < array.length; j++) {
            array[j][j] = 1;
            array[j][array[j].length - 1 - j] = 1;
        }
        return array;
    }

    public static int[][] getArrayB(int arrayLength) {
        int[][] array = new int[arrayLength][arrayLength];
        for (int j = 0; j < array.length; j++) {
            array[j][j] = 1;
            array[j][array[j].length - 1 - j] = 1;
            array[j][array[j].length / 2] = 1;
        }
        for (int i = 0; i < array[0].length; i++) {
            array[array.length / 2][i] = 1;
        }
        return array;
    }

    public static int[][] getArrayC(int arrayLength) {
        int[][] array = new int[arrayLength][arrayLength];
        for (int j = 0; j < array.length; j++) {
            for (int i = 0; i < array[j].length; i++) {
                if (i >= j && i <= array[j].length - 1 - j) {
                    array[j][i] = 1;
                    array[array.length - 1 - j][i] = 1;
                }
            }
        }
        return array;
    }

    private static void printArray(int[][] array) {
        for (int[] firstArrayElement : array) {
            for (int secondArrayElement : firstArrayElement) {
                System.out.print(secondArrayElement + " ");
            }
            System.out.println();
        }
    }
}
