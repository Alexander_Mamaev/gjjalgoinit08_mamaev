package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh02N031 {
    public static void main(String[] args) {
        System.out.println("Please enter a number with changed order.");
        System.out.println(getInitialNumberOrder(readInt(100, 999)));
    }

    public static int getInitialNumberOrder(int number) {
        int x = number / 100;
        int y = number % 10;
        int z = (number / 10) % 10;
        return x * 100 + y * 10 + z;
    }
}
