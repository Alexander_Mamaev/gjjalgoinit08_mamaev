package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh12N234 {
    public static void main(String[] args) {
        int[][] array = new int[][]{
                {10, 2, 10, 20},
                {2, 2, 2, 2},
                {15, 2, 20, 15},
                {20, 2, 10, 20}
        };
        printArray(array);
        printArray(delRaw(array, 2));
        printArray(delColumn(array, 2));
    }

    public static int[][] delRaw(int[][] array, int rawNumber) {
        int[][] delRawArray = new int[array.length][array[0].length];
        for (int j = 0, k = 0; j < array.length; j++, k++) {
            if (j == rawNumber - 1) {
                j++;
            }
            System.arraycopy(array[j], 0, delRawArray[k], 0, array[j].length);
        }
        return delRawArray;
    }

    public static int[][] delColumn(int[][] array, int colNumber) {
        int[][] delCalArray = new int[array.length][array[0].length];
        for (int i = 0, k = 0; i < array[0].length; i++, k++) {
            if (i == colNumber - 1) {
                i++;
            }
            for (int j = 0; j < array.length; j++) {
                delCalArray[j][k] = array[j][i];
            }
        }
        return delCalArray;
    }

    private static void printArray(int[][] array) {
        for (int[] arrayElement : array) {
            for (int element : arrayElement) {
                System.out.print(element + " ");
            }
            System.out.println();
        }
        System.out.println("---------------------");
    }
}
