package com.getjavajob.training.algo.init.mamaeva;

import java.util.Scanner;

public class TaskCh09N107 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Please enter a word.");
        String word = scanner.nextLine();
        System.out.println(changeWord(word));
    }

    public static String changeWord(String word) {
        int indexFirstA = 0;
        int indexLastO = 0;
        boolean firstAIdentified = false;
        boolean lastOIdentified = false;
        char[] wordArray = word.toCharArray();

        for (int i = 0; i < wordArray.length; i++) {
            if (!firstAIdentified && wordArray[i] == 'a') {
                indexFirstA = i;
                firstAIdentified = true;
            } else if (wordArray[i] == 'o') {
                indexLastO = i;
                lastOIdentified = true;
            }
        }
        if (firstAIdentified && lastOIdentified) {
            wordArray[indexFirstA] = 'o';
            wordArray[indexLastO] = 'a';
            return new String(wordArray);
        } else {
            System.out.println("Both 'a' and 'o' character not found in the entered word!");
            return word;
        }
    }
}
