package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh04N067.dayType;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh04N067Test {
    public static void main(String[] args) {
        dayTypeTest(5, "Workday");
        dayTypeTest(7, "Weekend");
        dayTypeTest(140, "Weekend");
        dayTypeTest(365, "Workday");
        printTestResult();
    }

    private static void dayTypeTest(int dayNumber, String expectedResult) {
        String actualResult = dayType(dayNumber);
        String assertMessage = "TaskCh04N067: The day number " + dayNumber + " is...";
        assertEquals(assertMessage, expectedResult, actualResult);
    }
}
