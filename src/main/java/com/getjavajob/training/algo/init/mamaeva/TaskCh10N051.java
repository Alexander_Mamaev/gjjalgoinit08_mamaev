package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N051 {
    public static void main(String[] args) {
        procedure1(5);
        System.out.println();
        procedure2(5);
        System.out.println();
        procedure3(5);
    }

    private static void procedure1(int n) {
        if (n > 0) {
            System.out.print(n);
            procedure1(n - 1);
        }
    }

    private static void procedure2(int n) {
        if (n > 0) {
            procedure2(n - 1);
            System.out.print(n);
        }
    }

    private static void procedure3(int n) {
        if (n > 0) {
            System.out.print(n);
            procedure3(n - 1);
            System.out.print(n);
        }
    }
}
