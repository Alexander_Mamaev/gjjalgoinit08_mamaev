package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N042 {
    public static double pow(double number, int power) {
        if (power == 1) {
            return number;
        } else {
            return number * pow(number, --power);
        }
    }
}
