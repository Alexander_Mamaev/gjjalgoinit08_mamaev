package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh02N013.getInverseNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh02N013Test {
    public static void main(String[] args) {
        testInverseNumber(123, 321);
        testInverseNumber(101, 101);
        testInverseNumber(155, 551);
        testInverseNumber(192, 291);
        testInverseNumber(120, 21);
        testInverseNumber(180, 81);
        testInverseNumber(10, 1);
        printTestResult();
    }

    private static void testInverseNumber(int originalNumber, int expectedInverseNumber) {
        int actualNumber = getInverseNumber(originalNumber);
        String assertMessage = "TaskCh02N013: " + originalNumber + " --> " + actualNumber;
        assertEquals(assertMessage, expectedInverseNumber, actualNumber);
    }
}
