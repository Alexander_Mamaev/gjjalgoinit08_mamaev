package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N048 {
    public static int getMaxArrayElement(int[] array) {
        return getMaxArrayElement(array, array.length);
    }

    private static int getMaxArrayElement(int[] array, int currentIndexPosition) {
        if (currentIndexPosition == 1) {
            return array[0];
        } else {
            int temp = getMaxArrayElement(array, --currentIndexPosition);
            return array[currentIndexPosition] > temp ? array[currentIndexPosition] : temp;
        }
    }
}

