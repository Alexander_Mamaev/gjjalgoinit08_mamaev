package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N043 {
    public static int getDigitsSum(int n) {
        if (n < 10) {
            return n;
        } else {
            return getDigitsSum(n / 10) + n % 10;
        }
    }

    public static int getDigitsQuantity(int n) {
        if (n < 10) {
            return 1;
        } else {
            return getDigitsQuantity(n / 10) + 1;
        }
    }
}

