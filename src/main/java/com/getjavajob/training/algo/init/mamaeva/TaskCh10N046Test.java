package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N046.getGeometricProgressionSum;
import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N046.getGeometricProgressionTerm;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N046Test {
    public static void main(String[] args) {
        getGeometricProgressionTermTest(1, 2, 5, 16);
        getGeometricProgressionTermTest(5, 5, 3, 125);
        getGeometricProgressionSumTest(1, 2, 5, 31);
        getGeometricProgressionSumTest(5, 5, 3, 155);
        printTestResult();
    }

    private static void getGeometricProgressionTermTest(int firstTerm, int progressionDenominator, int nTerm, int expectedResult) {
        assertEquals("TaskCh10N046: getGeometricProgressionTermTest: ", expectedResult, getGeometricProgressionTerm(firstTerm, progressionDenominator, nTerm));
    }

    private static void getGeometricProgressionSumTest(int firstTerm, int progressionDenominator, int nTerm, int expectedResult) {
        assertEquals("TaskCh10N046: getGeometricProgressionSumTest: ", expectedResult, getGeometricProgressionSum(firstTerm, progressionDenominator, nTerm));
    }
}
