package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N044.getDigitalRoot;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N044Test {
    public static void main(String[] args) {
        getDigitalRootTest(99, 9);
        getDigitalRootTest(0, 0);
        getDigitalRootTest(123, 6);
        getDigitalRootTest(789, 6);
        printTestResult();
    }

    private static void getDigitalRootTest(int n, int expectedResult) {
        assertEquals("TaskCh10N044: getDigitalRootTest: ", expectedResult, getDigitalRoot(n));
    }
}
