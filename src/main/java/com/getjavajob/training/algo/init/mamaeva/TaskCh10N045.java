package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N045 {
    public static int getArithmeticProgressionTerm(int firstTerm, int difference, int nTerm) {
        if (nTerm == 1) {
            return firstTerm;
        } else {
            return getArithmeticProgressionTerm(firstTerm, difference, --nTerm) + difference;
        }
    }

    public static int getArithmeticProgressionSum(int firstTerm, int difference, int nTerm) {
        if (nTerm == 1) {
            return firstTerm;
        } else {
            return getArithmeticProgressionTerm(firstTerm, difference, nTerm)
                    + getArithmeticProgressionSum(firstTerm, difference, --nTerm);
        }
    }
}
