package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh12N063 {
    public static int[] getAverageSchoolboyTable(int[][] schoolboyTable) {
        int[] schoolboysAverageTable = new int[schoolboyTable.length];
        for (int j = 0; j < schoolboyTable.length; j++) {
            int schoolboysSum = 0;
            for (int i = 0; i < schoolboyTable[0].length; i++) {
                schoolboysSum += schoolboyTable[j][i];
            }
            schoolboysAverageTable[j] = schoolboysSum / schoolboyTable[j].length;
        }
        return schoolboysAverageTable;
    }

}
