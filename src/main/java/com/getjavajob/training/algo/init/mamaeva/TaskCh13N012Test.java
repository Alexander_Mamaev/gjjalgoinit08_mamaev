package com.getjavajob.training.algo.init.mamaeva;

import java.util.ArrayList;
import java.util.Collection;

import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh13N012Test {
    public static void main(String[] args) {
        Database database = new Database(loadEmployeeList());
        System.out.println(database.getEmployeeWorkDurationList(20));
        System.out.println();
        getEmployeeWorkDurationTest(20, getExpectedEmployeeWorkDurationList(database), database);
        System.out.println();
        System.out.println(database.findEmployee("zlo"));
        System.out.println();
        findEmployeeTest("zlo", getExpectedFindEmployee(database), database);

        printTestResult();
    }

    private static Collection<Employee> loadEmployeeList() {
        Collection<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee("Ivan", "Ivanov", "Ivanovich", "Ivanovo 45", 1, 1991));
        employeeList.add(new Employee("Petr", "Petrov", "Petrovo 33", 2, 1992));
        employeeList.add(new Employee("Sidor", "Sidorov", "Sidorovich", "Sidorovo 22", 3, 1993));
        employeeList.add(new Employee("Rost", "Rostov", "Rostovo 44", 4, 1994));
        employeeList.add(new Employee("Kozl", "Kozlov", "Kozlovich", "Kozlovo 23", 5, 1995));
        employeeList.add(new Employee("Drozd", "Drozdov", "Drozdovich", "Drozdovo 11", 5, 1996));
        employeeList.add(new Employee("Abram", "Abramov", "Abramovich", "Abramovo 34", 7, 1996));
        employeeList.add(new Employee("IVANIK", "IVANIKOV", "IVANOVIKOVICH", "Ivanovo 46", 8, 1997));
        employeeList.add(new Employee("PETRIK", "PETRIKOV", "PETRIKOVICH", "petrikovo 33", 9, 1998));
        employeeList.add(new Employee("Bakozlik", "Bakozlov", "Kozlovo 24", 10, 1999));
        employeeList.add(new Employee("Pavel", "Pavlov", "Pavlovich", "Pavlovo 33", 11, 2000));
        employeeList.add(new Employee("Adel", "Adelov", "Adelovo 11", 12, 2001));
        employeeList.add(new Employee("Rolan", "Rolanov", "Rolanovich", "Rolanovo", 1, 2002));
        employeeList.add(new Employee("Kim", "Kimov", "Kimovo", 2, 2003));
        employeeList.add(new Employee("Sidor", "Begemotov", "Begemotovo 1", 3, 2004));
        employeeList.add(new Employee("Luka", "Lukov", "Lukovich", "Lukovo 1d", 4, 2005));
        employeeList.add(new Employee("Lev", "Lvov", "Lvovo 34", 5, 2006));
        employeeList.add(new Employee("Taras", "Tarasov", "Tarasovich", 6, 2007));
        employeeList.add(new Employee("Savva", "Savv", "Savich", "Savichevo 78", 7, 2009));
        employeeList.add(new Employee("Begemot", "Begemotov", "Begemotovich", "Begemotovo 34", 8, 2010));
        return employeeList;
    }

    /**
     * @return List of employees hwo works more than 20 years;
     */
    private static Collection<Employee> getExpectedEmployeeWorkDurationList(Database database) {
        Collection<Employee> list = new ArrayList<>();
        list.add(database.getEmployee("Ivan", "Ivanov", "Ivanovich", "Ivanovo 45", 1, 1991));
        list.add(database.getEmployee("Petr", "Petrov", "", "Petrovo 33", 2, 1992));
        list.add(database.getEmployee("Sidor", "Sidorov", "Sidorovich", "Sidorovo 22", 3, 1993));
        list.add(database.getEmployee("Rost", "Rostov", "", "Rostovo 44", 4, 1994));
        list.add(database.getEmployee("Kozl", "Kozlov", "Kozlovich", "Kozlovo 23", 5, 1995));
        list.add(database.getEmployee("Drozd", "Drozdov", "Drozdovich", "Drozdovo 11", 5, 1996));
        return list;
    }

    /**
     * @return List of employees with keyPhrase "zlo"
     */
    private static Collection<Employee> getExpectedFindEmployee(Database database) {
        Collection<Employee> list = new ArrayList<>();
        list.add(database.getEmployee("Kozl", "Kozlov", "Kozlovich", "Kozlovo 23", 5, 1995));
        list.add(database.getEmployee("Bakozlik", "Bakozlov", "", "Kozlovo 24", 10, 1999));
        return list;
    }

    private static void getEmployeeWorkDurationTest(int workDuration, Collection<Employee> expected, Database database) {
        assertEquals("TaskCh13N012: getEmployeeWorkDurationTest: ", expected, database.getEmployeeWorkDurationList(workDuration));
    }

    private static void findEmployeeTest(String keyPhrase, Collection<Employee> expected, Database database) {
        assertEquals("TaskCh13N012: findEmployeeTest: ", expected, database.findEmployee(keyPhrase));
    }
}
