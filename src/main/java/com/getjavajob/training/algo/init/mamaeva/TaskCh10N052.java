package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N052 {
    public static int reverseInteger(int number) {
        return reverseInteger(number, 0);
    }

    private static int reverseInteger(int number, int temp) {
        if (number < 10) {
            return temp + number;
        } else {
            temp = 10 * (temp + number % 10);
            return reverseInteger(number / 10, temp);
        }
    }
}

