package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh04N015.getPersonAge;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh04N015Test {
    public static void main(String[] args) {
        testGetPersonAge(12, 2014, 6, 1985, 29);
        testGetPersonAge(5, 2014, 6, 1985, 28);
        testGetPersonAge(6, 2014, 6, 1985, 29);
        printTestResult();
    }

    private static void testGetPersonAge(int currentMonth, int currentYear, int birthdayMonth, int birthdayYear, int expectedAge) {
        int actualAge = getPersonAge(currentMonth, currentYear, birthdayMonth, birthdayYear);
        assertEquals("TaskCh04N015: ", expectedAge, actualAge);
    }
}

