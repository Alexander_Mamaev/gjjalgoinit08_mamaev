package com.getjavajob.training.algo.init.mamaeva;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class TaskCh13N012 {
    public static void main(String[] args) {
        Database database = new Database(employeeList());
        System.out.println(database.getEmployeeWorkDurationList(18));
        System.out.println(database.findEmployee("zlo"));
        System.out.println(database.findEmployee("rov"));
        System.out.println(database.findEmployee("ich"));
    }

    private static Collection<Employee> employeeList() {
        Collection<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee("Ivan", "Ivanov", "Ivanovich", "Ivanovo 45", 1, 1991));
        employeeList.add(new Employee("Petr", "Petrov", "Petrovo 33", 2, 1992));
        employeeList.add(new Employee("Sidor", "Sidorov", "Sidorovich", "Sidorovo 22", 3, 1993));
        employeeList.add(new Employee("Rost", "Rostov", "Rostovo 44", 4, 1994));
        employeeList.add(new Employee("Kozl", "Kozlov", "Kozlovich", "Kozlovo 23", 5, 1995));
        employeeList.add(new Employee("Drozd", "Drozdov", "Drozdovich", "Drozdovo 11", 4, 1996));
        employeeList.add(new Employee("Abram", "Abramov", "Abramovich", "Abramovo 34", 7, 1996));
        employeeList.add(new Employee("IVANIK", "IVANIKOV", "IVANOVIKOVICH", "Ivanovo 46", 8, 1997));
        employeeList.add(new Employee("PETRIK", "PETRIKOV", "PETRIKOVICH", "petrikovo 33", 9, 1998));
        employeeList.add(new Employee("Bakozlik", "Bakozlovih", "Kozlovo 24", 10, 1999));
        employeeList.add(new Employee("Pavel", "Pavlov", "Pavlovich", "Pavlovo 33", 11, 2000));
        employeeList.add(new Employee("Adel", "Adelov", "Adelovo 11", 12, 2001));
        employeeList.add(new Employee("Rolan", "Rolanov", "Rolanovich", "Rolanovo", 1, 2002));
        employeeList.add(new Employee("Kim", "Kimov", "Kimovo", 2, 2003));
        employeeList.add(new Employee("Sidor", "Begemotov", "Begemotovo 1", 3, 2004));
        employeeList.add(new Employee("Luka", "Lukov", "Lukovich", "Lukovo 1d", 4, 2005));
        employeeList.add(new Employee("Lev", "Lvov", "Lvovo 34", 5, 2006));
        employeeList.add(new Employee("Taras", "Tarasov", "Tarasovich", 6, 2007));
        employeeList.add(new Employee("Savva", "Savv", "Savich", "Savichevo 78", 7, 2009));
        employeeList.add(new Employee("Begemot", "Begemotov", "Begemotovich", "Begemotovo 34", 8, 2010));
        return employeeList;
    }
}

class Database {
    private Collection<Employee> employeeList = new ArrayList<>();

    public Database(Collection<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    private Collection<Employee> getEmployeeList() {
        return employeeList;
    }

    public Collection<Employee> getEmployeeWorkDurationList(int workDuration) {
        Collection<Employee> selectionList = new ArrayList<>();
        for (Employee e : getEmployeeList()) {
            if (e.getWorkDuration() >= workDuration) {
                selectionList.add(e);
            }
        }
        return selectionList;
    }

    public Employee getEmployee(String name, String lastName, String patronymic, String address, int month, int year) {
        for (Employee e : employeeList) {
            if (e.getName().equals(name) && e.getLastName().equals(lastName) && e.getAddress().equals(address)
                    && e.getPatronymic().equals(patronymic) && e.getMonth() == month && e.getYear() == year) {
                return e;
            }
        }
        return null;
    }

    public Collection<Employee> findEmployee(String key) {
        Collection<Employee> keyEmployee = new ArrayList<>();
        key = key.toLowerCase();
        for (Employee e : getEmployeeList()) {
            if (e.getName().toLowerCase().contains(key) ||
                    e.getLastName().toLowerCase().contains(key) ||
                    e.getPatronymic().toLowerCase().contains(key)) {
                keyEmployee.add(e);
            }
        }
        return keyEmployee;
    }
}

class Employee {
    private String name;
    private String lastName;
    private String patronymic;
    private String address;
    private int month;
    private int year;

    public Employee(String name, String lastName, String address, int month, int year) {
        this.name = name;
        this.lastName = lastName;
        this.address = address;
        this.month = month;
        this.year = year;
    }

    public Employee(String name, String lastName, String patronymic, String address, int month, int year) {
        this(name, lastName, address, month, year);
        this.patronymic = patronymic;
    }

    public int getWorkDuration() {
        Date currentDate = new Date();
        int duration = currentDate.getYear() - this.getYear() + 1900;
        return currentDate.getMonth() >= this.getMonth() - 1 ? duration : duration - 1;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        if (patronymic == null) {
            return "";
        }
        return patronymic;
    }

    public String getAddress() {
        return address;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return getName() + " " + getLastName() + " " + getPatronymic() + ". Address: " + getAddress() + ". Date: " +
                getMonth() + "." + getYear() + System.lineSeparator();
    }
}