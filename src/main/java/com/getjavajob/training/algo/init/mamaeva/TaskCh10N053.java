package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N053 {
    public static int[] getReverseSequence(int[] array) {
        return getReverseSequence(array, new int[array.length - 1], 0);
    }

    private static int[] getReverseSequence(int[] sequence, int[] temp, int index) {
        if (sequence[index] == 0) {
            return temp;
        } else {
            temp[temp.length - index - 1] = sequence[index];
            return getReverseSequence(sequence, temp, ++index);
        }
    }
}
