package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh04N106 {
    public static void main(String[] args) {
        System.out.println("Please enter a number of month (1-12).");
        int monthNumber = readInt(1, 12);
        System.out.println("The month number " + monthNumber + " corresponded to... " + season(monthNumber));
    }

    public static String season(int monthNumber) {
        String season = "";
        switch (monthNumber) {
            case 12:
            case 1:
            case 2:
                season = "Winter";
                break;
            case 3:
            case 4:
            case 5:
                season = "Spring";
                break;
            case 6:
            case 7:
            case 8:
                season = "Summer";
                break;
            case 9:
            case 10:
            case 11:
                season = "Autumn";
                break;
            default:
                System.out.println("Something wrong.");
                break;
        }
        return season;
    }
}
