package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N042.pow;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N042Test {
    public static void main(String[] args) {
        powTest(0, 1, 0);
        powTest(1, 1, 1);
        powTest(2, 1, 2);
        powTest(2, 2, 4);
        powTest(10, 6, 1000000);
        printTestResult();
    }

    private static void powTest(double number, int power, int expectedResult) {
        assertEquals("TaskCh10N042: ", expectedResult, pow(number, power));
    }
}
