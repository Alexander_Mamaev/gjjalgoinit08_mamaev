package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh12N063.getAverageSchoolboyTable;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh12N063Test {
    public static void main(String[] args) {
        int[][] testTable = new int[][]{
                {10, 20, 10, 20},
                {15, 10, 20, 15},
                {15, 10, 20, 15},
                {20, 10, 10, 20},
                {15, 15, 15, 15},
                {20, 20, 10, 10},
                {10, 10, 20, 20},
                {10, 20, 10, 20},
                {15, 15, 20, 10},
                {0, 0, 0, 20},
                {0, 0, 0, 0},
        };
        int[] expectedResult = {15, 15, 15, 15, 15, 15, 15, 15, 15, 5, 0};
        getAverageSchoolboyTableTest(testTable, expectedResult);
        printTestResult();
    }

    private static void getAverageSchoolboyTableTest(int[][] testTable, int[] expectedResult) {
        assertEquals("TaskCh12N063: getAverageSchoolboyTableTest: ", expectedResult, getAverageSchoolboyTable(testTable));
    }
}
