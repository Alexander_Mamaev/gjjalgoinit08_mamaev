package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh12N023.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh12N023Test {
    public static void main(String[] args) {
        int[][] arrayA = new int[][]{
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        };
        int[][] arrayB = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        };
        int[][] arrayC = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        };

        getArrayTest(getArrayA(arrayA.length), arrayA);
        getArrayTest(getArrayB(arrayB.length), arrayB);
        getArrayTest(getArrayC(arrayC.length), arrayC);
        printTestResult();
    }

    private static void getArrayTest(int[][] array, int[][] expectedResult) {
        assertEquals("TaskCh12N023: getArrayTest: ", expectedResult, array);
    }
}
