package com.getjavajob.training.algo.init.mamaeva;

import java.util.Scanner;

public class TaskCh09N042 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Please enter a word.");
        String word = scanner.nextLine();
        System.out.println("Reversed word: " + reverseWord(word));
    }

    public static String reverseWord(String word) {
        StringBuilder stringBuilder = new StringBuilder(word);
        return stringBuilder.reverse().toString();
    }
}
