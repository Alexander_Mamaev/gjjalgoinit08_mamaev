package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh03N029.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh03N029Test {
    public static void main(String[] args) {
        System.out.println("TaskA: each of the numbers X and Y is odd.");
        taskATest(2, 2, false);
        taskATest(3, 2, false);
        taskATest(2, 3, false);
        taskATest(3, 3, true);

        System.out.println();
        System.out.println("TaskB: Only one of the numbers X and Y is less than 20.");
        taskBTest(10, 30, true);
        taskBTest(20, 20, false);
        taskBTest(10, 10, false);

        System.out.println();
        System.out.println("TaskC: at least one of the numbers X and Y equal to zero.");
        taskCTest(0, 0, true);
        taskCTest(2, 0, true);
        taskCTest(0, 2, true);
        taskCTest(2, 2, false);

        System.out.println();
        System.out.println("TaskD: each of the numbers X, Y, Z are negative.");
        taskDTest(-1, -1, -1, true);
        taskDTest(0, -1, -1, false);
        taskDTest(-1, 0, -1, false);
        taskDTest(-1, -1, 0, false);
        taskDTest(-1, 0, 0, false);
        taskDTest(0, 0, 0, false);

        System.out.println();
        System.out.println("TaskE: Only one of the numbers X, Y and Z is multiple of 5.");
        taskETest(25, 23, 23, true);
        taskETest(25, 25, 23, false);
        taskETest(25, 25, 25, false);
        taskETest(23, 25, 25, false);
        taskETest(23, 25, 23, true);
        taskETest(23, 23, 25, true);
        taskETest(23, 23, 23, false);

        System.out.println();
        System.out.println("TaskF: at least one of the numbers X, Y, Z is more than 100.");
        taskFTest(200, 2, 2, true);
        taskFTest(200, 200, 2, true);
        taskFTest(200, 200, 200, true);
        taskFTest(2, 200, 200, true);
        taskFTest(2, 2, 200, true);
        taskFTest(2, 2, 2, false);

        printTestResult();
    }

    private static void taskATest(int x, int y, boolean expectedResult) {   //each of the numbers X and Y is odd
        boolean actualResult = taskA(x, y);
        assertEquals("TaskCh03N029: ", expectedResult, actualResult);
    }

    private static void taskBTest(int x, int y, boolean expectedResult) {   //Only one of the numbers X and Y is less than 20.
        boolean actualResult = taskB(x, y);
        assertEquals("TaskCh03N029: ", expectedResult, actualResult);
    }

    private static void taskCTest(int x, int y, boolean expectedResult) {   //at least one of the numbers X and Y equal to zero
        boolean actualResult = taskC(x, y);
        assertEquals("TaskCh03N029: ", expectedResult, actualResult);
    }

    private static void taskDTest(int x, int y, int z, boolean expectedResult) {   //each of the numbers X, Y, Z are negative.
        boolean actualResult = taskD(x, y, z);
        assertEquals("TaskCh03N029: ", expectedResult, actualResult);
    }

    private static void taskETest(int x, int y, int z, boolean expectedResult) {   //Only one of the numbers X, Y and Z is multiple of 5.
        boolean actualResult = taskE(x, y, z);
        assertEquals("TaskCh03N029: ", expectedResult, actualResult);
    }

    private static void taskFTest(int x, int y, int z, boolean expectedResult) {   //at least one of the numbers X, Y, Z is more than 100.
        boolean actualResult = taskF(x, y, z);
        assertEquals("TaskCh03N029: ", expectedResult, actualResult);
    }
}
