package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh05N064 {
    public static void main(String[] args) {
        int[][] populationAndArea = {
                {12, 60},
                {11, 110},
                {10, 50},
                {9, 90},
                {8, 40},
                {7, 70},
                {6, 30},
                {5, 50},
                {4, 20},
                {3, 30},
                {2, 10},
                {1, 10}
        };
        System.out.println("The average population density is... " + averagePopulationDensity(populationAndArea));
    }

    public static double averagePopulationDensity(int[][] populationAndArea) {
        double populationDensity = 0;
        for (int[] arrayElement : populationAndArea) {
            populationDensity += (double) arrayElement[0] / arrayElement[1];
        }
        return populationDensity / populationAndArea.length;
    }

}
