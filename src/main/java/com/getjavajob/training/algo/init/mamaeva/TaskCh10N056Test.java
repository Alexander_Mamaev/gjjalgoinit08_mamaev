package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N056.isPrimeNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N056Test {
    public static void main(String[] args) {
        isPrimeNumberTest(0, false);
        isPrimeNumberTest(1, false);
        isPrimeNumberTest(2, true);
        isPrimeNumberTest(3, true);
        isPrimeNumberTest(4, false);
        isPrimeNumberTest(5, true);
        isPrimeNumberTest(6, false);
        isPrimeNumberTest(7, true);
        isPrimeNumberTest(8, false);
        isPrimeNumberTest(9, false);
        isPrimeNumberTest(10, false);
        isPrimeNumberTest(11, true);
        isPrimeNumberTest(12, false);

        printTestResult();
    }

    private static void isPrimeNumberTest(int number, boolean expectedResult) {
        assertEquals("TaskCh10N056: isPrimeNumberTest: ", expectedResult, isPrimeNumber(number));
    }
}
