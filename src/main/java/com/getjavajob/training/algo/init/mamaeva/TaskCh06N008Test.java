package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh06N008.numberSequenceUntilNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh06N008Test {
    public static void main(String[] args) {
        int[] numberSequence = {1, 4, 9, 16, 25, 36};
        numberSequenceTest(39, numberSequence);
        printTestResult();
    }

    private static void numberSequenceTest(int number, int[] expectedResult) {
        int[] actualResult = numberSequenceUntilNumber(number);
        String assertMessage = "TaskCh06N008: The number sequence until " + number + "... ";
        assertEquals(assertMessage, expectedResult, actualResult);
    }
}
