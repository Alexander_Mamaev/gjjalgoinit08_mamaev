package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N056 {
    public static boolean isPrimeNumber(int number) {
        return isPrimeNumber(number, 2);
    }

    private static boolean isPrimeNumber(int number, int i) {
        return number == 2 || number > 2 && number % i != 0 && (i >= number / 2 || isPrimeNumber(number, ++i));
    }
}