package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh09N166 {
    public static void main(String[] args) {
        String string1 = "АРГЕНТИНА МАНИТ НЕГРА";
        String string2 = "ПОТ КАК ПОТОП";
        String string3 = "А РОЗА УПАЛА НА ЛАПУ АЗОРА";
        System.out.println(string1);
        System.out.println(isPhraseReversed(string1));
        System.out.println(string2);
        System.out.println(isPhraseReversed(string2));
        System.out.println(string3);
        System.out.println(isPhraseReversed(string3));

    }

    public static boolean isPhraseReversed(String phrase) {
        String[] phraseArray = phrase.split(" ");
        String splitPhrase = "";
        for (String words : phraseArray) {
            splitPhrase = splitPhrase.concat(words);
        }
        return splitPhrase.equals(new StringBuilder(splitPhrase).reverse().toString());
    }
}
