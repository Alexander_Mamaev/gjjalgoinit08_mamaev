package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh05N010 {
    public static void main(String[] args) {
        double[] dollarTable = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        System.out.println("Please enter dollar exchange rate (1$ = ... RUB)");
        double exchangeRate = readInt();
        double[][] dollarExchangeTable = dollarExchangeTable(dollarTable, exchangeRate);
        for (double[] arrayElement : dollarExchangeTable) {
            for (int i = 0; i < arrayElement.length; i++) {
                System.out.print(arrayElement[i]);
                if (i == 0) {
                    System.out.print("-->");
                }
            }
            System.out.println();
        }
    }

    public static double[][] dollarExchangeTable(double[] dollarTable, double exchangeRate) {
        double[][] dollarExchangeTable = new double[dollarTable.length][2];
        for (int i = 0; i < dollarTable.length; i++) {
            dollarExchangeTable[i][0] = dollarTable[i];
            dollarExchangeTable[i][1] = dollarTable[i] * exchangeRate;
        }
        return dollarExchangeTable;
    }
}
