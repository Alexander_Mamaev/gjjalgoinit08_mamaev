package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh11N158.delDuplicates;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh11N158Test {
    public static void main(String[] args) {
        delDuplicatesTest(new int[]{1, 0, 2, 3, 1, 4, 0, 1, 6, 1, 7, 8},
                new int[]{1, 0, 2, 3, 4, 6, 7, 8, 0, 0, 0, 0});

        delDuplicatesTest(new int[]{8, 1, 8, 2, 8, 3, 8, 4, 8, 5, 8, 6},
                new int[]{8, 1, 2, 3, 4, 5, 6, 0, 0, 0, 0, 0});

        delDuplicatesTest(new int[]{1, 1, 0, 0, 3, 3, 2, 2, 0, 0, 1, 1},
                new int[]{1, 0, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0});

        printTestResult();
    }

    private static void delDuplicatesTest(int[] array, int[] expectedResult) {
        assertEquals("TaskCh11N158: delDuplicatesTest: ", expectedResult, delDuplicates(array));
    }
}
