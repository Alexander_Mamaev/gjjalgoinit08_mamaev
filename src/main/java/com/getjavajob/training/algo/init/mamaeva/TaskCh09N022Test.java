package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh09N022.halfWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh09N022Test {
    public static void main(String[] args) {
        halfWordTest("word", "wo");
        halfWordTest("as", "a");
        printTestResult();
    }

    private static void halfWordTest(String word, String expectedResult) {
        String assertMessage = "TaskCh09N017: Word \"" + word + "\"";
        assertEquals(assertMessage, expectedResult, halfWord(word));
    }
}
