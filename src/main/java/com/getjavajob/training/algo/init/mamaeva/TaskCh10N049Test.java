package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N49.getIndexOfMaxArrayElement;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N049Test {
    public static void main(String[] args) {
        getMaxElementTest(new int[]{5, 3, 2}, 0);
        getMaxElementTest(new int[]{1, 2, 3, 4, 5, 11, 6, 7, 8, 9}, 5);
        getMaxElementTest(new int[]{0, 1, 5, 2, 10}, 4);
        printTestResult();
    }

    private static void getMaxElementTest(int[] array, int expectedResult) {
        assertEquals("TaskCh10N049: getIndexOfMaxArrayElementTest: ", expectedResult, getIndexOfMaxArrayElement(array));
    }
}
