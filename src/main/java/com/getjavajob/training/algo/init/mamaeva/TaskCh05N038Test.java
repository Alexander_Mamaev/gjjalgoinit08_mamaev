package com.getjavajob.training.algo.init.mamaeva;

import java.math.BigDecimal;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh05N038.allManDistance;
import static com.getjavajob.training.algo.init.mamaeva.TaskCh05N038.distanceBetweenManAndHome;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh05N038Test {
    public static void main(String[] args) {
        distanceBetweenManAndHomeTest(0, 1);
        distanceBetweenManAndHomeTest(1, 0.5);
        distanceBetweenManAndHomeTest(5, 0.6167);
        distanceBetweenManAndHomeTest(100, 0.698);

        allManDistanceTest(0, 1);
        allManDistanceTest(1, 1.5);
        allManDistanceTest(5, 2.45);
        allManDistanceTest(100, 5.197);

        printTestResult();
    }

    private static void allManDistanceTest(int turnsValue, double expectedResult) {
        double actualRoundResult = new BigDecimal(allManDistance(turnsValue)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double expectedRoundResult = new BigDecimal(expectedResult).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        String assertMessage = "TaskCh05N03: allManDistanceTest after " + turnsValue + " turns...";
        assertEquals(assertMessage, expectedRoundResult, actualRoundResult);
    }

    private static void distanceBetweenManAndHomeTest(int turnsValue, double expectedResult) {
        double actualRoundResult = new BigDecimal(distanceBetweenManAndHome(turnsValue)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double expectedRoundResult = new BigDecimal(expectedResult).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        String assertMessage = "TaskCh05N038: distanceBetweenManAndHomeTest after  " + turnsValue + " turns...";
        assertEquals(assertMessage, expectedRoundResult, actualRoundResult);
    }
}
