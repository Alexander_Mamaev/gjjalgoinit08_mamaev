package com.getjavajob.training.algo.init.mamaeva;

import java.util.Scanner;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh09N015 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Please enter a word.");
        String word = scanner.nextLine();
        System.out.println("Please enter the serial number of the character in the word.");
        int serialNumber = readInt(1, word.length());
        System.out.println(charAtPosition(word, serialNumber));
    }

    public static char charAtPosition(String word, int serialNumber) {
        return word.charAt(serialNumber - 1);
    }
}
