package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh12N028.getArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh12N028Test {
    public static void main(String[] args) {
        int[][] testArray = new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };
        getArrayTest(testArray.length, testArray[0].length, testArray);
        printTestResult();
    }

    private static void getArrayTest(int arrayHeight, int arrayWidth, int[][] expectedResult) {
        assertEquals("TaskCh12N028: getArrayTest: ", expectedResult, getArray(arrayHeight, arrayWidth));
    }
}
