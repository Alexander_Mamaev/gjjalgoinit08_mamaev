package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh10N053.getReverseSequence;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh10N053Test {
    public static void main(String[] args) {
        getReverseSequenceTest(new int[]{1, 2, 3, 4, 5, 0}, new int[]{5, 4, 3, 2, 1});
        printTestResult();
    }

    private static void getReverseSequenceTest(int[] array, int[] expectedResult) {
        assertEquals("TaskCh10N053: getReverseSequence: ", expectedResult, getReverseSequence(array));
    }
}
