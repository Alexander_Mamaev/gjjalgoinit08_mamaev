package com.getjavajob.training.algo.init.mamaeva;

import java.util.Scanner;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh06N087 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Game game = new Game();
        System.out.println("Please enter the name of first team.");
        game.setTeam1(new Team(scanner.nextLine()));
        System.out.println("Please enter the name of second team.");
        game.setTeam2(new Team(scanner.nextLine()));
        game.play();
        game.printGameResult();
    }
}

class Game {

    private Team team1;
    private Team team2;
    private int team1Score;
    private int team2Score;

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public int getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(int team1Score) {
        this.team1Score += team1Score;
    }

    public int getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(int team2Score) {
        this.team2Score += team2Score;
    }

    public void play() {
        while (true) {
            System.out.println("Please enter the team that make a goal now.");
            System.out.println("1 - " + getTeam1().getTeamName());
            System.out.println("2 - " + getTeam2().getTeamName());
            int teamNumber = readInt(1, 2);
            System.out.println("Please enter value of take points (1 - 3).");
            System.out.println("Enter 0 to end the game.");
            int takePoints = readInt(0, 3);

            if (takePoints == 0) {
                break;
            } else if (teamNumber == 1) {
                setTeam1Score(takePoints);
            } else {
                setTeam2Score(takePoints);
            }
            System.out.printf("Game score: %s VS %s. %s : %s%n", getTeam1().getTeamName(),
                    getTeam2().getTeamName(), getTeam1Score(), getTeam2Score());
        }
    }

    public void printGameResult() {
        if (getTeam1Score() == getTeam2Score()) {
            System.out.println(team1.getTeamName() + " VS " + team2.getTeamName() + " played draw game "
                    + getTeam1Score() + " : " + getTeam2Score());
        } else if (getTeam1Score() > getTeam2Score()) {
            System.out.println(team1.getTeamName() + " won " + team2.getTeamName() + " with a score of "
                    + getTeam1Score() + " : " + getTeam2Score());
        } else {
            System.out.println(team2.getTeamName() + " won " + team1.getTeamName() + " with a score of "
                    + getTeam2Score() + " : " + getTeam1Score());
        }
    }

    /**
     * @return array[winner, loser, winner score, loser score]
     * loser/winner: 1 - firstTeam, 2 - secondTeam, 0 - drawGame
     */
    public int[] getGameResult() {
        int[] result = new int[4];
        if (getTeam1Score() == getTeam2Score()) {
            result[0] = 0;
            result[1] = 0;
            result[2] = getTeam1Score();
            result[3] = getTeam2Score();
        } else if (getTeam1Score() > getTeam2Score()) {
            result[0] = 1;
            result[1] = 2;
            result[2] = getTeam1Score();
            result[3] = getTeam2Score();
        } else {
            result[0] = 2;
            result[1] = 1;
            result[2] = getTeam2Score();
            result[3] = getTeam1Score();
        }
        return result;
    }
}

class Team {
    private String teamName;

    public Team(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamName() {
        return teamName;
    }
}