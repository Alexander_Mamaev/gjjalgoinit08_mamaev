package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.TaskCh11N245.getChangedArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static com.getjavajob.training.algo.util.Assert.printTestResult;

public class TaskCh11N245Test {
    public static void main(String[] args) {
        getChangedArrayTest(new int[]{5, -1, 2, -2, 3, 0, -3, -4, 4, 1}, new int[]{-1, -2, -3, -4, 1, 4, 0, 3, 2, 5});
        printTestResult();
    }

    private static void getChangedArrayTest(int[] array, int[] expectedResult) {
        assertEquals("TaskCh11N245: getChangedArrayTest: ", expectedResult, getChangedArray(array));
    }
}
