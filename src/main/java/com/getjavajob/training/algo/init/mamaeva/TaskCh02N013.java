package com.getjavajob.training.algo.init.mamaeva;

import static com.getjavajob.training.algo.init.mamaeva.ConsoleInput.readInt;

public class TaskCh02N013 {
    public static void main(String[] args) {
        System.out.println("Please enter a number greater than 99 and smaller than 1000");
        System.out.println(getInverseNumber(readInt(100, 999)));
    }

    public static int getInverseNumber(int n) {
        int result = 0;
        while (n > 0) {
            result = result * 10 + n % 10;
            n /= 10;
        }
        return result;
    }
}