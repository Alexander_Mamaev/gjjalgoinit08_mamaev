package com.getjavajob.training.algo.init.mamaeva;

public class TaskCh10N044 {
    public static int getDigitalRoot(int n) {
        if (n < 10) {
            return n;
        } else {
            return getDigitsSum(getDigitsSum(n));
        }
    }

    private static int getDigitsSum(int n) {
        if (n < 10) {
            return n;
        } else {
            return getDigitsSum(n / 10) + n % 10;
        }
    }
}
