package com.getjavajob.training.algo.init.mamaeva;

import java.util.Scanner;

public class TaskCh09N022 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Please enter a word.");
        String word = scanner.nextLine();
        System.out.println(halfWord(word));
    }

    public static String halfWord(String word) {
        return word.substring(0, word.length() / 2);
    }
}
