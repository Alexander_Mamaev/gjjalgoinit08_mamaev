package com.getjavajob.training.algo.util;

import java.util.Arrays;
import java.util.Collection;

public class Assert {

    private static boolean testPassed = true;

    private static boolean isTestPassed() {
        return testPassed;
    }

    private static void setTestFailed() {
        testPassed = false;
    }

    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed: expected " + expected + ", actual " + actual);
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed: expected " + expected + ", actual " + actual);
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, char expected, char actual) {
        if (expected == actual) {
            System.out.printf("%s passed: expected %c, actual %c%n", testName, expected, actual);
        } else {
            System.out.printf("%s failed: expected %c, actual %c%n", testName, expected, actual);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed: expected " + expected + ", actual " + actual);
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed: expected " + expected + ", actual " + actual);
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, double[][] expected, double[][] actual) {
        if (expected.length != actual.length) {
            setTestFailed();
            return;
        } else {
            for (int j = 0; j < expected.length; j++) {
                if (expected[j].length != actual[j].length) {
                    setTestFailed();
                    return;
                }
            }
        }
        System.out.println(testName + System.lineSeparator() + "expected ... actual");
        for (int j = 0; j < expected.length; j++) {
            for (int i = 0; i < expected[j].length; i++) {
                if (expected[j][i] != actual[j][i]) {
                    setTestFailed();
                }
                printArray(expected[j]);
                System.out.print("... ");
                printArray(actual[j]);
                System.out.println();
            }
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        if (expected.length != actual.length) {
            setTestFailed();
            System.out.println("The arrays have not equal length for comparison!");
            return;
        } else {
            for (int i = 0; i < expected.length; i++) {
                if (expected[i] != actual[i]) {
                    setTestFailed();
                }
            }
        }
        System.out.print(testName + System.lineSeparator() + "expected ... ");
        printArray(expected);
        System.out.println();
        System.out.print("actual ..... ");
        printArray(actual);
        System.out.println();
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            System.out.println(testName + " passed.");
        } else {
            System.out.println(testName + " failed.");
        }
    }

    public static void assertEquals(String testName, Collection expected, Collection actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed.");
        } else {
            System.out.println(testName + " failed.");
        }
    }

    private static void printArray(int[] array) {
        for (int arrayElement : array) {
            System.out.print(arrayElement + " ");
        }
    }

    private static void printArray(double[] array) {
        for (double arrayElement : array) {
            System.out.print(arrayElement + " ");
        }
    }

    public static void printTestResult() {
        System.out.println();
        System.out.println("TEST " + (isTestPassed() ? "PASSED" : "FAILED"));
    }
}
